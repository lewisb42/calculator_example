from pybuilder.core import use_plugin
from pybuilder.core import init
from pybuilder.core import Author

# plugins for common tasks
use_plugin('python.core')
use_plugin('python.unittest')
use_plugin('python.install_dependencies')
use_plugin('python.distutils')
use_plugin('python.integrationtest')

# the task run when pyb is executed
default_task = 'publish'

@init
def config_integrationtest(project):
    project.set_property('integrationtest_inherit_environment', True)

# extra configuration
@init
def initialize(project):
    # project dependencies
    project.depends_on('selenium')

    # project information
    project.name = 'Web Calculator Example'
    project.version = '1.0'
    project.summary = 'Browser-based calculator'
    project.description = '''Browser-based calculator, with e2e tests, based on the original by Gheorghe Marin Adrian'''
    project.authors = [Author('Lewis Baumstark', 'lewisb@westga.edu'), Author('Gheorghe Marin Adrian', '')]
    project.license = "not for redistribution"
    project.url = 'https://www.westga.edu/academics/cosm/computer-science/'
