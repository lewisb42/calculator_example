import unittest
from selenium import webdriver
from subprocess import Popen
import os


class SimpleE2ETests(unittest.TestCase):
    
    # starts the webdriver-controlled Firefox process
    def startBrowser(self):
        ffoptions = webdriver.firefox.options.Options()
        ffoptions.headless = True
        self.driver = webdriver.Firefox(options=ffoptions)
    
    # starts our application (which is a customized web server)
    def startWebServer(self):
        # runs the command (as a subprocess):
        # $ python src/main/python/app.py
        appPath = os.path.join('src', 'main', 'python', 'app.py')
        self.serverProc = Popen(['python', appPath])
        
    # this method is run before *every* test
    def setUp(self):
        self.startWebServer()
        self.startBrowser()

    # this method is run after *every* test
    def tearDown(self):
        self.driver.close()
        self.serverProc.kill()
        
    # an actual test
    def testShouldHaveAnH1(self):
        # load the page
        self.driver.get("http://127.0.0.1:8000/simple.html")
        # find the element with the id of 'hello' (our H1)
        h1 = self.driver.find_element_by_id('hello')
        # assert it has the expected text
        self.assertEqual("Greetings, Program!", h1.text)
        
# pybuilder doesn't run integration tests as unittest, instead it just runs the module
# so we need to tell it to run as regular unittest
if __name__ == '__main__':
	unittest.main()
