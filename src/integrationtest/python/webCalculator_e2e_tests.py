import unittest
from selenium import webdriver
from subprocess import Popen
import os


class WebCalculatorE2ETests(unittest.TestCase):
    

    def startBrowser(self):
        ffoptions = webdriver.firefox.options.Options()
        ffoptions.headless = True
        self.driver = webdriver.Firefox(options=ffoptions)
        self.resource = "http://127.0.0.1:8000/webCalculator.html"
        
    def startWebServer(self):
        appPath = os.path.join('src', 'main', 'python', 'app.py')
        self.serverProc = Popen(['python', appPath])
        
    def setUp(self):
        self.startWebServer()
        self.startBrowser()


    def tearDown(self):
        self.driver.close()
        self.serverProc.kill()

    def testShouldAddUsingButtons(self):
        self.driver.get(self.resource)
        # press the '2' key
        twoKey = self.driver.find_element_by_id('twoKey')
        twoKey.click()

        # press the '+' key
        plusKey = self.driver.find_element_by_id('plusKey')
        plusKey.click()

        # press the '4' key
        fourKey = self.driver.find_element_by_id('fourKey')
        fourKey.click()

        # press the '=' key (calculates the result)
        equalsKey = self.driver.find_element_by_id('equalsKey')
        equalsKey.click();

        # check the displayed result is correct
        result = self.driver.find_element_by_id('answer').get_attribute('value')
        self.assertEqual("6", result)

    def testShouldAddUsingKeyboardEntry(self):
        self.driver.get(self.resource)

        # manually type in an expression
        display = self.driver.find_element_by_id('answer')
        display.send_keys("10*8")

        # press the '=' key (calculates the result)
        equalsKey = self.driver.find_element_by_id('equalsKey')
        equalsKey.click();

        # check the displayed result is correct
        result = self.driver.find_element_by_id('answer').get_attribute('value')
        self.assertEqual("80", result)

# pybuilder doesn't run integration tests as unittest, instead it just runs the module
# so we need to tell it to run as regular unittest
if __name__ == '__main__':
	unittest.main()
